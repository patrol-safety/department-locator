package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"os"

	"gitlab.com/bdgo/golog"

	"googlemaps.github.io/maps"
)

var err error
var client *maps.Client

func init() {
	client, err = maps.NewClient(maps.WithAPIKey(os.Getenv("GOOGLE_MAPS_API_KEY")))
	if err != nil {
		log.Fatalf("fatal error: %s", err)
	}
}

func getNearByDepartments(ctx context.Context, address string) maps.PlacesSearchResponse {
	gcr := maps.GeocodingRequest{
		Address: address,
	}

	r, err := client.Geocode(ctx, &gcr)
	if err != nil {
		panic(err)
	}

	loc := r[0].Geometry.Location

	nsr := maps.NearbySearchRequest{
		Location: &loc,
		RankBy:   "distance",
		Type:     "police",
	}

	rnsr, err := client.NearbySearch(ctx, &nsr)
	if err != nil {
		panic(err)
	}

	return rnsr
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	address := r.FormValue("address")
	if address == "" {
		golog.Print(ctx, "Error", "Empty address field")
		http.NotFound(w, r)

		return
	}

	golog.Print(ctx, "Request", address)

	departments := getNearByDepartments(ctx, address)

	b, err := json.Marshal(departments)
	check(err)

	w.Write([]byte(b))
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	log.Println("Starting...")
	http.HandleFunc("/", golog.DecorateHandler(handleRequest))
	http.ListenAndServe(":8181", nil)
}
