module gitlab.com/patrol-safety/department-locator

go 1.13

require (
	github.com/google/uuid v1.1.1 // indirect
	gitlab.com/bdgo/golog v1.0.0
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	googlemaps.github.io/maps v0.0.0-20191014172202-ce2e58e026c5
)
